<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_nnnotifications_domain_model_log'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_nnnotifications_domain_model_log']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'recipient',
	),
	'types' => array(
		'1' => array('showitem' => 'recipient'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'tstamp' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

		'mid' => array(
			'exclude' => 1,
			'label' => 'Titel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		
		'error' => array(
			'exclude' => 1,
			'label' => 'Fehler',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		
		'recipient' => Array (
			'label' => 'Empfänger (E-Mail oder Device-Token)',
			'config' => Array (
				'type' => 'text',
			)
		),
		
	),
);