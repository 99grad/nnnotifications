<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_nnnotifications_domain_model_queue'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_nnnotifications_domain_model_queue']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'title',
	),
	'types' => array(
		'1' => array('showitem' => 'title,subject,predef,transport,data_provider,content_provider,recipient_provider,sendparams,executioninterval,lastexecution,totalsent'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'tstamp' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        'crdate' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        
		'title' => array(
			'exclude' => 1,
			'label' => 'Titel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		
		'subject' => array(
			'exclude' => 1,
			'label' => 'Titel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		
		'tstamp' => Array (
			'label' => 'Erstellungsdatum',
			'config' => Array (
				'type' => 'input',
				'eval' => 'datetime',
			)
		),
		
		'template' => Array (
			'label' => 'Template',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'predef' => Array (
			'label' => 'Setup-Typ (predef)',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'transport' => Array (
			'label' => 'Transport',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'data_provider' => Array (
			'label' => 'Data-Provider',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'content_provider' => Array (
			'label' => 'Content-Provider',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'recipient_provider' => Array (
			'label' => 'Recipient-Provider',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'sendparams' => Array (
			'label' => 'Parameter',
			'config' => Array (
				'type' => 'text',
			)
		),
		
		'executioninterval' => Array (
			'label' => 'Interval der Wiederholung (in Sekunden)',
			'config' => Array (
				'type' => 'input',
			)
		),
		
		'lastexecution' => Array (
			'label' => 'Letzte Aussendung',
			'config' => Array (
				'type' => 'input',
			)
		),
		'countrecipients' => Array (
			'label' => 'Zahl Empfänger',
			'config' => Array (
				'type' => 'input',
			)
		),
		'totalsent' => Array (
			'label' => 'Versendet',
			'config' => Array (
				'type' => 'input',
			)
		),
		
	),
);