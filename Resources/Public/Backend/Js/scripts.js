
(function($){$(function () {


	function redbind_elements () {
	
		$('[data-toggle="popover"]').popover({trigger:'hover', delay:0, container: 'body'});


		// Benutzer möchte Eintrag löschen
		$('a.delete').unbind().click( function () {
			var data = $(this).data();
			if (!data.repeated && !data.additionals) {
				$('.modal').data({ref:$(this)});
				$('#modal-delete-single').modal();
			} else {
				$('.modal').data({ref:$(this)});
				$('#modal-delete-reference').modal();
			}
			return false;
		});
		
		$('a.approve').unbind().click( function () {
			var data = $(this).data();
			$('.modal').data({ref:$(this)});
			$('#modal-approve-single').modal();
			return false;
		});
		
	}
	
	redbind_elements();
	

	// ... und sagt: "einzelnes Event löschen"
	$('#modal-delete-single .delete-single').click( function () {
		$(this).closest('.modal').data().ref.closest('.row').delay(200).fadeOut(1000);
		var urls = $(this).closest('.modal').data().ref.data();
		ajax_action( urls.deleteSingle, true );
	});
	
	
	// ... und sagt: "einzelnes Event löschen"
	$('#modal-approve-single .approve-single').click( function () {
		$(this).closest('.modal').data().ref.closest('.row').addClass('approved');
		var urls = $(this).closest('.modal').data().ref.data();
		ajax_action( urls.approveSingle, false );
	});
	

	function ajax_action ( url, refresh ) {
		$('.all-items').addClass('spinning');
		$.ajax( url ).done( function ( data ) {
			if (refresh === false) return;
			$('.all-items').html( $('.all-items', data).html() );
			$('.all-items').removeClass('spinning');
			redbind_elements();
		});
	}
	


});})(jQuery);

