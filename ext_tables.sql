

#
# Table structure for table 'tx_nnnotifications_domain_model_queue'
#
CREATE TABLE tx_nnnotifications_domain_model_queue (
	uid int(11) unsigned NOT NULL auto_increment,
	pid int(11) unsigned DEFAULT '0' NOT NULL,
	
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,

	type tinyint(4) unsigned DEFAULT '0' NOT NULL,
	subject varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	sendparams mediumblob NOT NULL,
	
	predef varchar(255) DEFAULT '' NOT NULL,
	template varchar(255) DEFAULT '' NOT NULL,
	transport varchar(255) DEFAULT '' NOT NULL,
	data_provider varchar(255) DEFAULT '' NOT NULL,
	content_provider varchar(255) DEFAULT '' NOT NULL,
	recipient_provider varchar(255) DEFAULT '' NOT NULL,

	countrecipients int(11) unsigned DEFAULT '0' NOT NULL, 
	totalsent int(11) unsigned DEFAULT '0' NOT NULL,
	lastexecution int(11) unsigned DEFAULT '0' NOT NULL,
	executioninterval int(11) unsigned DEFAULT '0' NOT NULL,
	
	PRIMARY KEY (uid)
);


#
# Table structure for table 'tx_nnnotifications_domain_model_log'
#
CREATE TABLE tx_nnnotifications_domain_model_log (
	uid int(11) unsigned NOT NULL auto_increment,
	mid int(11) unsigned DEFAULT '0' NOT NULL,
	recipient varchar(255) DEFAULT '' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	error int(11) unsigned DEFAULT '0' NOT NULL,
	PRIMARY KEY (uid),
	KEY `mid` (mid)
);




