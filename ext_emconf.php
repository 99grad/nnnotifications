<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "nnnotifications".
 *
 * Auto generated 09-06-2015 18:02
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Notification Center',
	'description' => 'Zentrales System zur Benachrichtigung von Usern',
	'category' => 'misc',
	'version' => '1.0.0',
	'state' => 'beta',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'David Bascom',
	'author_email' => 'david@99grad.de',
	'author_company' => '99grad',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '6.2.0-7.1.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

