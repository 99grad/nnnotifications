<?php
namespace Nng\Nnnotifications\Controller;


/**
 * ModController
 */
 
class ModController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * @var \Nng\Nnnotifications\Helper\AnyHelper
	 * @inject
	 */
	protected $anyHelper;
	
	/**
	 * @var \Nng\Nnnotifications\Domain\Repository\QueueRepository
	 * @inject
	 */
	protected $queueRepository = NULL;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;
	
	
	/**
	* @var \Nng\Nnnotifications\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	
	
	
	/**
	* Initializes the current action
	* @return void
	*/
	protected function initializeAction() {
				
		$this->cObj = $this->configurationManager->getContentObject();
		$this->settings = $this->settingsUtility->getTsSetup();
		$this->_GP = $this->request->getArguments();
	}

	/**
	* Initializes the current view
	* @return void
	*/
	protected function initializeView() {
		$this->view->assignMultiple(array(
			'settings'	=> $this->settings,
			'_GP'		=> $this->_GP,
			'cObjData'	=> $this->cObj->data,
			'baseURL'	=> $this->settingsUtility->getBaseURL(),
			'domain'	=> $this->settingsUtility->getDomain()
		));
	}
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function indexAction() {
		
		$data = $this->queueRepository->findAll();
		
		$this->view->assignMultiple(array(
			'queues' => $data
		));
		
	}


	/**
	 * action new
	 *
	 * @return void
	 */
	public function newAction() {
		
	}
	
	
	/**
	 * action delete
	 *
	 * @return void
	 */
	public function deleteAction() {
		
		if (!($entryUid = intval($this->_GP['uid']))) return;
		if (!($entry = $this->queueRepository->findByUid($entryUid))) return;
		$this->queueRepository->remove($entry);
		$this->persistenceManager->persistAll();
		
		$this->forward('index');
		return '';		
	}
	
	

	
}