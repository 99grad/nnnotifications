<?php

namespace Nng\Nnnotifications\Controller;

class EidController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	
	/**
	 * @var \Nng\Nnnotifications\Services\NotifcationTestService
	 * @inject
	 */
	protected $notifcationTestService;
	
	/**
	 * @var \Nng\Nnnotifications\Services\QueueService
	 * @inject
	 */
	protected $queueService;
	
	
	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	
	/**
	* Initializes the current action
	
	* @return void
	*/
	protected function initializeAction() {
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->mainController = $this->objectManager->create('\Nng\Nnnotifications\Services\NotifcationTestService');
		$this->queueService = $this->objectManager->create('\Nng\Nnnotifications\Services\QueueService');
	}
	
     /* 
     *	Wird von EidDispatcher.php aufgerufen, wenn in URL &eID=nnnotifications übergeben wurde
     *
     *	index.php?eID=nnnotifications&action=sendTestMail
     *	index.php?eID=nnnotifications&action=addTestToQueue
     *	index.php?eID=nnnotifications&action=processQueue
     */
	
	function processRequestAction () {
	
		$_GP = $this->request->getArguments();
		$action = $_GP['action'];		
			
		switch ($action) {
			case 'sendTestMail':
				$this->notifcationTestService->sendTestMail( $_GP );
				break;
			case 'addTestToQueue':
				$this->notifcationTestService->addTestToQueue( $_GP );
				break;			
			case 'createTestData':
				$this->notifcationTestService->createTestData( $_GP );
				break;	
			case 'performanceTest':
				$this->notifcationTestService->performanceTest( $_GP );
				break;	
			case 'processQueue':
				$this->queueService->processQueue();
				break;
			default:
				die("eID Action {$action} unknown.");
		}
		
		die();
		
	}

	function validateAction () {
		$_GP = $this->request->getArguments();
		$uid = (int) $_GP['uid'];
		$key = $_GP['key'];
		if (!$this->anyHelper->validateKeyForUid($uid, $key)) die("Validierung fehlgeschlagen.");
		return true;
	}

}


?>