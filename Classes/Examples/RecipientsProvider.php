<?php

namespace Nng\Nnnotifications\Examples;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class RecipientsProvider {

	/**	
	*	Liste von (Test-) Empfängern zurückgeben. Wird von QueueService aufgerufen
	*
	* 	@return array
	*/
	
	public function getRecipients ( $params = array(), $templateSettings = array() ) {
		
		$recipients = array('david@99grad.de', 'quatsch', 'facebook@99grad.de', 'booking@bluetinbox.de' );
		$recipients = array('david@99grad.de', 'quatsch');
		
		for ($i = 0; $i < 10; $i++) {
		
			/*
			// Beispiel (A): Rückgabe einer tablename ===> Dadurch direkte Abfrage des 
			// Datensatzes aus angegebener Tabelle. Siehe: QueueService->processQueue()
			$recipients[] = array(
				'tablename' => 'fe_users',
				'uid'		=> ($i+600),
				'recipient' => 'david-'.$i.'@99grad.de'
			);
			//*/
			
			/*
			// Beispiel (B): Rückgabe KEINER tablename ===> Dadurch wird von QueueService->processQueue()
			// für jeden Empfänger (falls angegeben) der dataProvider aufgerufen, s.o. addTestToQueue()
			$recipients[] = array(
				'uid'		=> ($i+600),
				'recipient' => 'david-'.$i.'@99grad.de'
			);
			//*/
			
			/*
			// Beispiel (C): Nur Rückgabe des Empfängers ===> Dadurch wird von QueueService->processQueue()
			// für jeden Empfänger (falls angegeben) der dataProvider aufgerufen, s.o. addTestToQueue()
			$recipients[] = 'david-'.$i.'@99grad.de';
			//*/
		}	
		return $recipients;
	}
	
}
		