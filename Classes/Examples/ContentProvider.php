<?php

namespace Nng\Nnnotifications\Examples;

use Nng\Nnnotifications\Provider\AbstractProvider;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class ContentProvider extends AbstractProvider {

	/**	
	*	Beispiel-Content zurückgeben. Wird von QueueService aufgerufen
	*
	* 	@return array
	*/

	public function getContent ( $params = array() ) {
		$html = $this->anyHelper->renderTemplate( $params['provider']['template'], $params );
		return $html;
	}

}
