<?php

namespace Nng\Nnnotifications\Examples;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataProvider {

	/**	
	*	Daten-Array für einzelne Nachricht zurückgeben. 
	*	Lädt z.B. die Daten aus fe_users, um die Benachrichtigung zu personalisieren.
	*	Wird von QueueService aufgerufen, die Daten werden danach an den contentProvider
	*	übergeben um das Fluid-Template rendern zu können
	*
	* 	@return array
	*/
	
	public function getDataForRecipient ( $params ) {
		return array('first_name'=>'David', 'last_name'=>'Bascom', 'email'=>$params['recipient']['recipient']);
	}
}