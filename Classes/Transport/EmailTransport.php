<?php

namespace Nng\Nnnotifications\Transport;

use Nng\Nnnotifications\Transport\AbstractTransport;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class EmailTransport extends AbstractTransport {

	/**
	 * @var \Nng\Nnnotifications\Services\EmailService
	 * @inject
	 */
	protected $emailService;
	
	
	/**	
	*	Sendet eine Nachricht per E-Mail. Wird von QueueService aufgerufen
	*
	* 	@return boolean
	*/
	
	public function send ( $params = array() ) {
		
//		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($params);

		$mailData = $params['config'];
		ArrayUtility::mergeRecursiveWithOverrule( $mailData, $params['content'] );
		ArrayUtility::mergeRecursiveWithOverrule( $mailData, array(
			'toEmail' 	=> $params['recipient']['recipient'],
			'toName' 	=> $params['recipient']['name'],
			'data'		=> $params['data']
		));
		
		$success = $this->emailService->send($mailData);
		return $success;
	}
	
}
		