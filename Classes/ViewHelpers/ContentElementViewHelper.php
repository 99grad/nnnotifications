<?php

namespace Nng\Nnnotifications\ViewHelpers;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 * ContentElementViewHelper
 */
class ContentElementViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;
	
	
    private $cObj;

    /**
     * Renders a content element
     *
     * @param int $uid
     * @param array $data
     *
     * @return string
     */

    public function render($uid, $data=NULL) {

        $this->cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
		
        $conf = array(
            'tables' => 'tt_content',
            'source' => $uid,
            'dontCheckPid' => 1
        );

        $html = $this->cObj->RECORDS($conf);
		$html = $GLOBALS['TSFE']->cObj->cObjGetSingle('RECORDS', $conf);
		
 		if ($data !== NULL) {
			$html = $this->renderTemplateSource($html, $data);
		}
		
        return $html ? $html : "Keine Content-Element mit uid {$uid} gefunden. Evtl. muss uid im View angepasst werden!";

    }

    /**
     * Renders a given code-snippet as fluid-template. Variables can be passed
     *
     * @param string $templateSrc
     * @param array $vars
     *
     * @return string
     */

    public function renderTemplateSource ($templateSrc=NULL, $vars) {

        if ($templateSrc === NULL) return '';

        $view = $this->objectManager->get('\TYPO3\CMS\Fluid\View\StandaloneView');
        $view->setTemplateSource($templateSrc);
        $view->assignMultiple($vars);
        return $view->render();
    }
	

}