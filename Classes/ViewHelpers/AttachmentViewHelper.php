<?php
namespace Nng\Nnnotifications\ViewHelpers;

class AttachmentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

 	/**
	 * @var \Nng\Nnnotifications\Utilities\SettingsUtility
	 * @inject
	 */
	protected $settingsUtility;
	
	/**
	 * Attach a file to eMail – inline or as Link
	 *
	 * @param string $src a path to a file
	 * @param boolean $embed add file as attachment to email
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render($src = NULL, $embed = true) {
	
		$baseURL = $this->settingsUtility->getBaseURL();
		
		if (!$GLOBALS['nnnotifications_attachments']) $GLOBALS['nnnotifications_attachments'] = array();
		if ($embed) $GLOBALS['nnnotifications_attachments'][] = $src;

		return $embed ? '' : $baseURL.str_replace($baseURL, '', $src);
	}
}
