<?php

namespace Nng\Nnnotifications\Tasks;


class SchedulerTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	
	public function execute () {
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$schedulerService = $objectManager->get('\Nng\Nnnotifications\Services\SchedulerService');
		$schedulerService->executeSchedulerTask();
		return true;
	}
	
}
		