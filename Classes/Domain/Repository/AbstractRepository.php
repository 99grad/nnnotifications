<?php

namespace Nng\Nnnotifications\Domain\Repository;

class AbstractRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	/**
	* @var \TYPO3\CMS\Core\Database\DatabaseConnection
	*/
	protected $_DB;
	
	
	public function initializeObject () {
		$this->_DB = $this->anyHelper->_DB();
		
		$querySettings = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings');
		$querySettings->setRespectStoragePage(FALSE);
		$this->setDefaultQuerySettings($querySettings);
	}

}
?>