<?php

namespace Nng\Nnnotifications\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class LogRepository extends \Nng\Nnnotifications\Domain\Repository\AbstractRepository {


	/**
	*	Logged einen Versand in der DB
	*
	* 	@return int
	*/
	public function log ( $queueUid, $recipient, $error = 0 ) {
	
		$row = array(
			'mid' 		=> $queueUid,
			'recipient'	=> $recipient,
			'error'		=> $error,
			'tstamp'	=> mktime()
		);
		GeneralUtility::addSlashesOnArray($row);
		$this->_DB->exec_INSERTquery( 'tx_nnnotifications_domain_model_log', $row );

		return $this->_DB->sql_insert_id();
		
	}
	
	/**
	*	Löschen aller Einträge für gegebene $queueUid
	*
	* 	@return void
	*/
	public function clearLogsForQueue ( $queueUid, $error = 0 ) {
		$this->_DB->exec_DELETEquery( 'tx_nnnotifications_domain_model_log', 'mid='.intval($queueUid) );
	}
	
	
	/**
	*	Update nach einem Fehler
	*
	* 	@return int
	*/
	public function setError ( $uid, $error = 0 ) {
		$this->_DB->exec_UPDATEquery( 'tx_nnnotifications_domain_model_log', 'uid='.intval($uid), array('error'=>$error) );
	}
	
	
	/**
	*	Filtert eine Liste von E-Mails: 
	*	Nur die E-Mails behalten, die nicht bereits in der Log-DB vorhanden sind
	*
	*	@var int $queueUid	 		Die uid der Notification aus tx_nnnotifications_domain_model_queue.uid
	*	@var array $recipientData 	Array mit Empfänger-Adressen
	*	
	* 	@return array				gefilterte Liste mit Empfängern, die keinen Log-Eintrag für queueID haben
	*/
	public function removeRecipientsIfLogged ( $queueUid, $recipientData = array() ) {

		$recipientArrByRecipient = array();	
		if (!$recipientData) return array();

		foreach ($recipientData as $k=>$recipient) {
			if (is_array($recipient)) {
				$recipientArrByRecipient[$recipient['recipient']] = $recipient;
			} else {
				if (is_numeric($k)) {
					$recipientArrByRecipient[$recipient] = array('recipient'=>$recipient);
				} else {
					$recipientArrByRecipient[$k] = array('recipient'=>$k, 'name'=>$recipient);
				}
			}
		}

		$keys = array_keys($recipientArrByRecipient);
		GeneralUtility::addSlashesOnArray($keys);
		$not = "'".join("','", $keys)."'";

		$filteredRecipients = array();
		
		// Um einen Speicher-Overflow zu vermeiden, werden die Ergebnisse seitenweise geladen
		$resultsPerQuery = 1000;
		$page = 0;
		$hadResults = true;
		
		while ($hadResults) {
			$result = $this->_DB->exec_SELECTgetRows(
				'recipient',
				'tx_nnnotifications_domain_model_log',
				'mid='.intval($queueUid). " AND recipient IN({$not})",
				'',	// group by
				'', // order by
				($page*$resultsPerQuery).','.($resultsPerQuery), // limit
				'recipient' // uid index field
			);
			$hadResults = $result && count($result) > 0;
			if ($hadResults) {
				foreach ($result as $k=>$v) {
					unset($recipientArrByRecipient[$k]);
				}
			}
			$page++;
		}
		
		return $recipientArrByRecipient;
		
		// Diese Methode wäre schöner, aber es gibt es max_length bei GROUP_CONCAT
		/*
		$result = $this->_DB->exec_SELECTgetRows(
			'GROUP_CONCAT(recipient) as recipients',
			'tx_nnnotifications_domain_model_log',
			'mid='.intval($queueUid). " AND recipient NOT IN({$not})",
			'',	// group by
			'', // order by
			'', // limit
			'' // uid index field
		);
		
		if (!$result || !count($result) || !$result[0]['recipients']) return array();
		
		$recipients = explode(',', $result[0]['recipients']);
		return $recipients;
		*/
		
	}
		
}
?>