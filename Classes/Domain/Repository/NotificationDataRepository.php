<?php

namespace Nng\Nnnotifications\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class NotificationDataRepository extends \Nng\Nnnotifications\Domain\Repository\AbstractRepository {
	
	
	public function findInTableByUid( $table = 'fe_users', $keyArr = array() ) {

		if (!$keyArr) return array();	
		if (is_int($keyArr)) $keyArr = array('uid'=>$keyArr);

		$table = $this->_DB->quoteStr($table, $table);
		
		$where = array();
		foreach ($keyArr as $key=>$uid) {
			$where[] = $this->_DB->quoteStr($key, $table).'='.$this->_DB->fullQuoteStr($uid, $table);
		}
		
		$enableFields = \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields($table);
		
		$rows = $this->_DB->exec_SELECTgetRows(
			'*',
			$table,
			join(' AND ', $where) . ' ' . $enableFields,
			'',	// group by
			'', // order by
			'1', // limit
			'' // uid index field
		);
		
		return $rows ? array_pop($rows) : array();
	}
	
}
?>