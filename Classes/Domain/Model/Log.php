<?php

namespace Nng\Nnnotifications\Domain\Model;


class Log extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {


	/**
	 * @var string
	 **/
	protected $recipient = "";

	/**
	 * @var int
	 **/
	protected $mid = "";
	
	/**
	 * @var int
	 **/
	protected $error = 0;
	
	/**
	 * @var string
	 **/
	protected $tstamp = "";
	
	
	public function setError($val) {
		$this->error = $val;
	}

	public function getError() {
		return $this->error;
	}
	
	public function setRecipient($val) {
		$this->recipient = $val;
	}

	public function getRecipient() {
		return $this->recipient;
	}
	
	public function setMid($val) {
		$this->mid = $val;
	}

	public function getMid() {
		return $this->mid;
	}
	
	public function setTstamp($val) {
		$this->tstamp = $val;
	}

	public function getTstamp() {
		return $this->tstamp;
	}
	
}
?>