<?php

namespace Nng\Nnnotifications\Domain\Model;


class Queue extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {


	/**
	 * @var string
	 **/
	protected $title = "";

	/**
	 * @var string
	 **/
	protected $subject = "";
	
	/**
	 * @var string
	 **/
	protected $tstamp = "";
	
	/**
	 * @var string
	 **/
	protected $lastexecution = "";
	
	/**
	 * @var string
	 **/
	protected $executioninterval = "";
	
	/**
	 * @var string
	 **/
	protected $totalsent = "";
	
	/**
	 * @var string
	 **/
	protected $countrecipients = "";

	/**
	 * @var string
	 **/
	protected $transport = "";
	
	/**
	 * @var string
	 **/
	protected $template = "";
	
	/**
	 * @var string
	 **/
	protected $predef = "";
	
	/**
	 * @var string
	 **/
	protected $dataProvider = "";
	
	/**
	 * @var string
	 **/
	protected $contentProvider = "";
	
	/**
	 * @var string
	 **/
	protected $recipientProvider = "";
	
	/**
	 * @var string
	 **/
	protected $sendparams = "";
	
	
	
	public function setLastexecution($val) {
		$this->lastexecution = $val;
	}

	public function getLastexecution() {
		return $this->lastexecution;
	}

	public function setExecutioninterval($val) {
		$this->executioninterval = $val;
	}

	public function getExecutioninterval() {
		return $this->executioninterval;
	}	
	
	public function setTotalsent($val) {
		$this->totalsent = $val;
	}

	public function getTotalsent() {
		return $this->totalsent;
	}
	
	public function setCountrecipients($val) {
		$this->countrecipients = $val;
	}

	public function getCountrecipients() {
		return $this->countrecipients;
	}
	
	public function setPredef($val) {
		$this->predef = $val;
	}

	public function getPredef() {
		return $this->predef;
	}
	
	public function setTransport($val) {
		$this->transport = $val;
	}

	public function getTransport() {
		return $this->transport;
	}
	
	public function setTemplate($val) {
		$this->template = $val;
	}

	public function getTemplate() {
		return $this->template;
	}
	
	public function setRecipientProvider($val) {
		$this->recipientProvider = $val;
	}

	public function getRecipientProvider() {
		return $this->recipientProvider;
	}
	
	public function setContentProvider($val) {
		$this->contentProvider = $val;
	}

	public function getDataProvider() {
		return $this->dataProvider;
	}
	
	public function setDataProvider($val) {
		$this->dataProvider = $val;
	}

	public function getContentProvider() {
		return $this->contentProvider;
	}
	
	public function setSendparams($val) {
		$this->sendparams = json_encode($val);
	}

	public function getSendparams() {
		return json_decode($this->sendparams, true);
	}
	
	public function setTitle($val) {
		$this->title = $val;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setSubject($val) {
		$this->subject = $val;
	}

	public function getSubject() {
		return $this->subject;
	}
	
	public function setTstamp($val) {
		$this->tstamp = $val;
	}

	public function getTstamp() {
		return $this->tstamp;
	}
	
}
?>