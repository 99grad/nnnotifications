<?php

namespace Nng\Nnnotifications\Provider;

use Nng\Nnnotifications\Provider\AbstractProvider;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class DataProvider extends AbstractProvider {
	
	/**	
	*	Liste von (Test-) Empfängern zurückgeben. Wird von QueueService aufgerufen
	*
	*	provider.where	=> z.B. [uid:'{uid}']
	*
	* 	@return array
	*/
	
	public function getFromTable ( $params ) {	
		
		if ($where = $params['provider']['where']) {
			
			$replace = array();
			foreach ($params['recipient'] as $k=>$v) {
				$replace['{field:'.$k.'}'] = $v;
			}
			
			$arr = array();
			foreach ($where as $k=>$v) {
				$where[$k] = str_replace(array_keys($replace), array_values($replace), $v);
			}
		} else if ($params['recipient']['uid']) {
			$where = array('uid' => $params['recipient']['uid']);
		} else {
			return array();
		}
			
		$data = $this->notificationDataRepository->findInTableByUid( $params['provider']['tablename'], $where );
		return $data;
	}
	
}
		