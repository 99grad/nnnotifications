<?php

namespace Nng\Nnnotifications\Provider;

use Nng\Nnnotifications\Provider\AbstractProvider;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class ContentProvider extends AbstractProvider {
	
	
	/**	
	*	Content zurückgeben. Wird von QueueService aufgerufen
	*
	* 	@return array
	*/

	public function getFromTemplate ( $params = array() ) {
		
		if (!($template = $params['provider']['template'])) {
			throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception("nnnotifications: contentProvider->getFromTemplate() - contentProvider.template nicht angegeben.");
		}
		
		$html = $this->anyHelper->renderTemplate( $template, $params );
		return array('html' => $html);
	}
	
}
		