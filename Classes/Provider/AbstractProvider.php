<?php

namespace Nng\Nnnotifications\Provider;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class AbstractProvider {

	/**
	* @var \Nng\Nnnotifications\Domain\Repository\NotificationDataRepository
	* @inject
	*/
	protected $notificationDataRepository;
	
	/**
	* @var \TYPO3\CMS\Extbase\Object\ObjectManager
	* @inject
	*/
	protected $objectManager;
	
	/**
	* @var \Nng\Nnnotifications\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	
	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
}
		