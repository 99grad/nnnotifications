<?php

namespace Nng\Nnnotifications\Utilities;

class CacheUtility extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;
	
	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Mvc\Request
	 * @inject
	 */
	protected $request;
	
	protected $cObj;
	protected $settings;
	protected $configuration;
	
	
	public function initializeObject () {
	/*
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->configurationManager = $objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManager');
		$this->request = $objectManager->get('\TYPO3\CMS\Extbase\Mvc\Request');
	*/	
		$this->cObj = $this->configurationManager->getContentObject();
		$this->configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$this->settings = $this->configuration['settings'];
		$this->cacheSettings = $this->settings['cache'];
		$this->gpVars = $this->request->getArguments();	
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Caching Functions
	// 
	// http://buzz.typo3.org/people/steffen-kamper/article/using-cache-in-extensions/
	//
	// funcName 	->	Functionsname, der einen Cache Wert holen oder setzen möchte (für eindeutige ID)
	// hashVars		->	String oder Array, der Parameter für Cache bestimmt (z.B. GET-Vars oder TS-Setup)
	// data			->	String / HTML-Code / Array der Daten, die gespeichert werden sollen
	
	
	public function getCache ( $funcName = null, $hashVars = null, $template = null ) {
		if ($this->cacheSettings['enabled'] === '0') return false;
		$id = $this->getCacheHash($hashVars, $funcName, $template);
		
		if ($data = $this->getRamCache($funcName, $hashVars, $template)) {
			$this->anyHelper->logFooter($funcName.' was loaded from RAM cache for id: '.$id);
			return $data;
		}
		
		$data = \TYPO3\CMS\Frontend\Page\PageRepository::getHash( $id );
		
		if (!$data) {
			/*
			print_r(array('hashVars'=>$hashVars, 'funcName'=>$funcName, 'template'=>$template));
			echo $this->getCacheHash($hashVars, $funcName, $template);
			*/
			$this->anyHelper->logFooter($funcName.' was NOT loaded from cache for id: '.$id);
			return false;
		}
		$data = unserialize($data);
		$this->anyHelper->logFooter($funcName.' was loaded from cache');
		return isset($data['__string']) ? $data['__string'] : $data;
	}


	public function setCache ( $data, $funcName = null, $hashVars = null, $template = null ) {
		/*
		print_r(array('hashVars'=>$hashVars, 'funcName'=>$funcName, 'template'=>$template));
		echo $this->getCacheHash($hashVars, $funcName, $template);
		*/
		
		$id = $this->getCacheHash($hashVars, $funcName, $template);
		
		$this->setRamCache($data, $funcName, $hashVars, $template);
		$this->anyHelper->addPageFooterData('<!-- nnnotifications: WRITE RAM cache for '.$funcName.' with id: '.$id.' -->');
		
		if ($this->cacheSettings['enabled'] === '0') {
			$this->anyHelper->addPageFooterData('<!-- nnnotifications: cache was disabled using "enabled = 0" -->');
			return $data;
		}
		$tmp = $data;
		if (!is_array($data)) $tmp = array('__string'=>$data);

		// Um Mitternacht endet der Cache
		$lifetime = $this->cacheSettings['clearAtMidnight'] ? mktime(23,59,59)+1-time() : 0;
		
		\TYPO3\CMS\Frontend\Page\PageRepository::storeHash( $id, serialize($tmp), $this->getCacheIdentifier($funcName), $lifetime );
		$this->anyHelper->addPageFooterData('<!-- nnnotifications: WRITE cache for '.$funcName.' with id: '.$id.' -->');
		return $data;
	}


	public function setRamCache ( $data, $funcName = null, $hashVars = null, $template = null ) {
		if (!$hashVars) $hashVars = array($funcName);
		if (!$data) $data = 'null';
		$GLOBALS[$this->getCacheHash($hashVars, $funcName, $template)] = $data;
		return $data;
	}

	public function getRamCache ( $funcName = null, $hashVars = null, $template = null ) {
		if (!$hashVars) $hashVars = array($funcName);
		return isset($GLOBALS[$this->getCacheHash($hashVars, $funcName, $template)]) ? $GLOBALS[$this->getCacheHash($hashVars, $funcName, $template)] : false;
	}


	private function getCacheHash ( $hashVars = null, $funcName = null, $template = null ) {
		if (!$funcName) $funcName = $this->viewType;
		if (!$hashVars) {
			$hashVars = array(
				'pid' 		=> $GLOBALS['TSFE']->id, 
				'lang' 		=> $GLOBALS['TSFE']->sys_language_uid,
				'uid' 		=> $this->cObj->data['uid'],
				'ffValues'	=> join(array_values($this->settings['flexform']))
			);			
			$hashVars = array_merge($hashVars, $this->gpVars);
		}
		
		if ($this->cacheSettings['period']) $hashVars['timestamp'] = ceil(mktime()/$this->cacheSettings['period']);
		if ($this->cacheSettings['clearAtMidnight']) $hashVars['date'] = date('Y-m-d');

		if ($hashVars['to']) {
			$hashVars['to'] = $this->cacheSettings['period'] ? ceil($hashVars['to']/$this->cacheSettings['period']) : date('Y-m-d', $hashVars['to']);
		}
		
		if (is_array($hashVars)) $hashVars = join('|',array_values($hashVars)).join('|', array_keys($hashVars));
		return '_'.md5($this->extKey.'-'.$hashVars.'-'.$funcName.'-'.$template);		
	}

	private function getCacheIdentifier( $funcName = null ) {
		if (!$funcName) $funcName = $this->viewtype;
		return $this->extKey.'-'.$funcName;
	}
	
}

?>