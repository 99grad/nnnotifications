<?php

namespace Nng\Nnnotifications\Helper;

class AnyHelper {

	
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;


	/**
	* @var \Nng\Nnnotifications\Utilities\CacheUtility
	* @inject
	*/
	protected $cacheUtility;
	
	
	/* 
	 *	Old-School piBase-Object erzeugen um alte Plugins zu initialisieren
	 *
	 */
	 
	function piBaseObj () {
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$configurationManager = $objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManager');
		$piObj = $objectManager->create('\TYPO3\CMS\Frontend\Plugin\AbstractPlugin');
		$piObj->cObj = $configurationManager->getContentObject();
		return $piObj;
	}
	
	
	function setPageTitle ( $titleStr ) {
		$GLOBALS['TSFE']->page['title'] = $titleStr;
		$GLOBALS['TSFE']->indexedDocTitle = $titleStr;
	}
	
	
	/* --------------------------------------------------------------- 
		LLL-Funktionen, Beispiele für $id:

		"LLL:EXT:nnnotifications/Resources/Private/Language/locallang_db.xlf:tx_nnnotifications_domain_model_repeat"
		oder: "weekdays_short" (locallang.xlf)
		
	*/
	
	public function getLL ( $id = 'weekdays_short', $explode = '', $extensionName = 'nnnotifications', $args = null ) {
	
		if ($val = $this->cacheUtility->getRamCache($id)) return $val;
		
		$value = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($id, $extensionName, $args );
		if ($explode) $value = \TYPO3\CMS\Extbase\Utility\ArrayUtility::trimExplode(($explode === true ? ',' : $explode), $value);
		
		return $this->cacheUtility->setRamCache($value, $id);
	}
	
	
	/* --------------------------------------------------------------- 
		Schlüssel erzeugen zur Validierung einer Abfrage
	*/
	
	function createKeyForUid ( $uid ) {
		$extConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['nnnotifications']);
		if ($extConfig['encodingKey'] == '99grad') die('<h1>nnfesubmit</h1><p>Bitte aendere die "Salting Key" in der Extension-Konfiguration auf etwas anderes als "99grad" (im Extension-Manager auf die Extension klicken)</p>');
		return substr(strrev(md5($uid.$extConfig['encodingKey'])), 0, 8);
	}
	
	function validateKeyForUid ( $uid, $key ) {
		return self::createKeyForUid( $uid ) == $key;
	}
	
	/* --------------------------------------------------------------- */

	
	function trimExplode ( $del, $str, $removeEmpty = true ) {
		if (is_array($str)) return $this->trimExplodeArray( $str, $removeEmpty );
		if (!trim($str)) return array();
		$str = explode($del, $str);
		foreach ($str as $k=>$v) $str[$k] = trim($v);
		return $str;
	}
	
	function trimExplodeArray ( $arr, $removeEmpty = true ) {
		if (!$arr) return array();
		if (!is_array($arr)) $arr = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $arr);
		$final = array();
		foreach ($arr as $n) {
			if (trim($n) || !$removeEmpty) $final[] = trim($n);
		}
		return $final;
	}
		
	/* --------------------------------------------------------------- */

	function calculate_images ( $data, $conf ) {
		$cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
		if (!$data) return array();
		if (!$data['image']) return array();
		if (!is_array($data['image'])) $data['image'] = explode(',', $data['image']);
		$imgs = array();
		foreach ($conf as $k=>$c) {
			$key = substr($k,0,-1);
			if (!$imgs[$key]) $imgs[$key] = array();
			foreach ($data['image'] as $img) {
				$c['file'] = 'uploads/pics/'.$img;
				$imgs[$key][] = $cObj->IMG_RESOURCE($c);
			}
		}
		return $imgs;
	}
	
	
	/* --------------------------------------------------------------- */

	function dropdown ( $arr, $selected = null, $name = null, $class = null ) {
		$tmp = array();
		foreach ($arr as $k=>$v) {
			$sel = $k == $selected ? ' selected="selected"' : '';
			$tmp[] = "<option value=\"{$k}\"{$sel}>{$v}</option>";
		}
		return "<select name=\"{$name}\" data-mid=\"{$class}\" class=\"mdata mdata-{$class}\">".join('', $tmp)."</select>";
	}
	
	
	public function renderTemplate ( $path, $vars = null, $pathPartials = null, $doubleRender = false ) {
				
		if (!$path) return '';
		if (!file_exists($path)) $path = realpath(PATH_site.$path);
		if (!file_exists($path)) return '';
		
		if (!$vars) $vars = array();
		if (is_object($vars)) $vars = $this->objectHelper->convertToArray($vars);
				
		$view = $this->objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');
		$view->setTemplatePathAndFilename($path);
		
		$allPartialPaths = $this->getPartialPaths( $path );
		if ($pathPartials) $allPartialPaths[] = $pathPartials;
		$view->setPartialRootPaths( $allPartialPaths );		

//		$this->logFile('CRON.txt', print_r($allPartialPaths, true) );
		
		$view->assignMultiple($vars);
		
		$html = $view->render();
		
		if ($doubleRender) {
			$view->setTemplateSource($html);
			$html = $view->render();
		}

		return $html;
	}
	
	
	public function renderTemplateSource ( $template, $vars, $pathPartials = null ) {
		
		if (!$template) return '';
		if (is_object($vars)) $vars = $this->objectHelper->convertToArray($vars);
		
		if (strpos($template, '{namespace') === false) {
			$template = '{namespace VH=Nng\Nnnotifications\ViewHelpers}'.$template;
		}
		
		$view = $this->objectManager->get('\TYPO3\CMS\Fluid\View\StandaloneView');		
		$view->setTemplateSource($template);
		
		$allPartialPaths = $this->getPartialPaths( $path );
		if ($pathPartials) $allPartialPaths[] = $pathPartials;
		$view->setPartialRootPaths( $allPartialPaths );		

		$view->assignMultiple( $vars );
		$html = $view->render();
		
		return $html;
	}
	
	
	public function getPartialPaths ( $template ) {

		$arr = array();
		$arr[] = str_replace(PATH_site, '', dirname($template).'/Partials/');
		$arr[] = PATH_site . str_replace(PATH_site, '', dirname($template).'/Partials/');
		
		for ($i = 0; $i < 3; $i++) {
			$arr[] = str_replace(PATH_site, '', realPath( dirname($template).'/'.str_repeat('../', $i) )).'/Partials/';
			$arr[] = str_replace(PATH_site, '', realPath( dirname($template).'/'.str_repeat('../', $i) )).'/';
			$arr[] = realPath( dirname($template).'/'.str_repeat('../', $i) ).'/Partials/';
			$arr[] = realPath( dirname($template).'/'.str_repeat('../', $i) ).'/';
		}
		//$arr[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnnotifications').'Resources/Private/Templates/Default/Mail/Partials/';
		//$arr[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnnotifications').'Resources/Private/Templates/Partials/';
		//$arr[] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnnotifications').'Resources/Private/Partials/';
		
		return $arr;
	}
	
	
	
	/* --------------------------------------------------------------- 
		JS / CSS einbinden
	*/
	
	public function addHeaderFile ( $fileArr, $includeAtTop = false, $compress = 1 ) {
	
		if (!$fileArr) return false;
		$pageRenderer = $GLOBALS['TSFE']->getPageRenderer();
		
		foreach ($fileArr as $k=>$v) {
			$suffix = pathinfo($v, PATHINFO_EXTENSION);
			if ($suffix == 'js') {
				if ($includeAtTop) {
					$pageRenderer->addJsFile($v, 'text/javascript', $compress);
				} else {
					//$GLOBALS['TSFE']->additionalHeaderData['nnnotifications'.md5($v)] = '<script type="text/javascript" src="'.$v.'"></script>';
					$pageRenderer->addJsLibrary(md5($v), $v, 'text/javascript', $compress);
				}
			}
			if ($suffix == 'css') {
				if ($includeAtTop) {
					$pageRenderer->addCssLibrary($v, 'stylesheet', $compress);
				} else {
					//$GLOBALS['TSFE']->additionalHeaderData['nnnotifications'.md5($v)] = '<link rel="stylesheet" type="text/css" media="all" href="'.$v.'" />';
					$pageRenderer->addCssFile($v, 'stylesheet', $compress);
				}
			}
		}
	}
	
	public function addPageFooterData ( $str ) {
		$pageRenderer = $GLOBALS['TSFE']->getPageRenderer();
		$pageRenderer->addFooterData($str);
	}
	
	
	public function logFooter ( $str = null ) {			
		$GLOBALS['_logcnt']++;
		$cnt = str_pad($GLOBALS['_logcnt'], 3, '0', STR_PAD_LEFT);
		$this->addPageFooterData("<!-- nnnotifications[{$cnt}]: ".$str." -->");
	}
	
	public function logTime ( $str = null, $resetTimer = true ) {
		if (!$GLOBALS['_now'] && $resetTimer) $GLOBALS['_now'] = microtime(true);
		if (!$str) return;
		$this->logFooter( (number_format(microtime(true)-$GLOBALS['_now'],3)) . " : {$str}");
		$GLOBALS['_now'] = microtime(true);
	}
		
	public function logFile ( $file, $message ) {
		$fp = fopen(PATH_site.$file, 'a');
		if ($fp) {
			fputs($fp, date('d.m.Y H:i:s')." {$message}\n");
			fclose($fp);
		}
	}

	public function simpleLog ( $str, $id = 'nnnotifications', $num = 0 ) {
		if (!$GLOBALS['BE_USER']) $GLOBALS['BE_USER'] = $GLOBALS['TSFE']->initializeBackendUser();
		if ($GLOBALS['BE_USER']) {
			$GLOBALS['BE_USER']->simplelog($str, $id, $num);
		}
	}
	
	public function _DB () {
		return $GLOBALS['TYPO3_DB'];
	}
	
	/* --------------------------------------------------------------- 
		NOTICE, ERROR, WARNING, OK
		$this->anyHelper->addFlashMessage('so,so', 'ja ja');

	*/
	
	function addFlashMessage ( $title = '', $text = '', $type = 'OK') {
		
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$controllerContext = $objectManager->create('\TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext');
		$controllerContext->getFlashMessageQueue()->enqueue(
			$objectManager->get( '\TYPO3\CMS\Core\Messaging\FlashMessage', $text, $title, constant('\TYPO3\CMS\Core\Messaging\FlashMessage::'.$type), false )
		);
	}
	
	function renderFlashMessages () {
		
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$controllerContext = $objectManager->create('\TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext');
		if (count($controllerContext->getFlashMessageQueue()->getAllMessages())) {
			return $this->renderTemplate('typo3conf/ext/nnnotifications/Resources/Private/Templates/FlashMessages.html', array() );
		}
		return '';
	}
	
	/* --------------------------------------------------------------- */
	

	
	function cloneArray( $arr ) {
		$ret = array();
		foreach ($arr as $k=>$v) $ret[$k] = $v;
		return $ret;
	}
	
	function cleanIntList ( $str='', $returnArray = null ) {
		$is_arr = is_array($str);
		if (trim($str) == '') return (($returnArray == null && !$is_arr) || $returnArr === false) ? '' : array();
		if ($is_arr) $str = join(',', $str);
		$str = $GLOBALS['TYPO3_DB']->cleanIntList( $str );
		if (($returnArray == null && !$is_arr) || $returnArr === false) return $str;
		return explode(',', $str);
	}
			
	function get_obj_by_attribute ( &$data, $key, $val = false, $retArr = false ) {
		$ref = array();
		foreach ($data as $k=>$v) {
			if ($val === false) {
				if ($retArr === true) {
					if (!is_array($ref[$v[$key]])) $ref[$v[$key]] = array();
					$ref[$v[$key]][] = &$data[$k];
				} else {
					$ref[$v[$key]] = &$data[$k];
				}
			} else {
				$ref[$v[$key]] = $val === true ? $v : $v[$val];
			}
		}
		return $ref;
	}
	
	
	// --------------------------------------------------------------------------------------------------------------------
	// Object-Funktionen
	
	/*
	*	Ruft eine Methode in einem Objekt auf
	*
	* 	@var string $funcStr  			=> z.B. \Nng\Nnnotifications\Services\NotifcationTestService->getTestRecipients
	*	@var array $params				=> Parameter, die an Funktion übergeben werden sollen
	*	@var array $params2				=> zweiter Parameter, der an Funktion übergeben werden sollen
	*	@var array $params3				=> dritter Parameter, der an Funktion übergeben werden sollen
	*/

	public function callClassFunction ( $funcStr, $params = array(), $params2 = null, $params3 = null ) {
		
		if (!trim($funcStr)) throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception("nnnotifications: callClassFunction() - Keine Klasse angegeben.");
		
		list($class, $method) = explode( '->', $funcStr );
		if (!class_exists($class)) throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception("nnnotifications: callClassFunction() - Klasse {$class} existiert nicht.");
		
		$classRef = $this->objectManager->get($class);
		if (!method_exists($classRef, $method)) throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception("nnnotifications: callClassFunction() - Methode {$class}->{$method}() existiert nicht.");

		if ($params3 != null) return $classRef->$method($params, $params2, $params3);		
		if ($params2 != null) return $classRef->$method($params, $params2);
		return $classRef->$method($params);
	}
		
	// --------------------------------------------------------------------------------------------------------------------
	// Weiterleitung über http-Header location:...
	
	static function httpRedirect ( $pid = null, $vars = array() ) {
		unset($vars['id']);
		if (!$pid) $pid = $GLOBALS['TSFE']->id;		
		$pi = self::piBaseObj();
		$link = $pi->pi_getPageLink($pid, '', $vars); 
		$link = \TYPO3\CMS\Core\Utility\GeneralUtility::locationHeaderUrl($link); 
		header('Location: '.$link); 
		exit(); 
	}

	
	static function sendToBrowser ( $path, $download = false, $filename = null ) {

		$isFile = realpath( $path ) !== '';
		if ($filename) $suffix = pathinfo($filename, PATHINFO_EXTENSION);
		if (!$suffix && $isFile) $suffix = pathinfo($path, PATHINFO_EXTENSION);
		if (!$suffix) $suffix = 'txt';
		if (!$isFile && !$filename) $filename = 'download';		
		if (!$filename) $filename = $isFile ? basename($path) : 'download.'.$suffix;

		ob_end_clean();
		if ($download) header('Content-disposition: attachment; filename='.$filename);
		header('Content-type: application/'.$suffix);
		
		if ($isFile) {
			//header('Content-Length: ' . filesize($path));
			readfile( $path );
		} else {
			//header('Content-Length: ' . strlen($path));
			echo $path;
		}
		die();
	}
	
}

?>