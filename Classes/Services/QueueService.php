<?php

namespace Nng\Nnnotifications\Services;

use Nng\Nnnotifications\Services\AbstractService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\DebugUtility;

class QueueService extends AbstractService {
	
	
	
	/**
	*	Legt einen neuen Queue für den Versand an
	*	Test: index.php?eID=nnnotifications&action=addTestToQueue
	*
	* 	@return \Nng\Nnnotifications\Domain\Model\Queue|false
	*/
	public function addToQueue( $params = array() ) {
		
		$queue = new \Nng\Nnnotifications\Domain\Model\Queue();
		
		$queue->setSendparams( $params['sendparams'] );
		$queue->setSubject( $params['subject'] );
		$queue->setTitle( $params['title'] );
		$queue->setContentProvider( $params['contentProvider'] );
		$queue->setRecipientProvider( $params['recipientProvider'] );
		$queue->setDataProvider( $params['dataProvider'] );
		$queue->setTemplate( $params['template'] );
		$queue->setTransport( $params['transport'] );
		$queue->setPredef( $params['predef'] );
		$queue->setExecutioninterval( $params['executioninterval'] );
		$queue->setLastexecution( $params['execute'] ? 0 : mktime() );
		
		$this->queueRepository->add( $queue);
		$this->persistenceManager->persistAll();
		
		$uid = $queue->getUid();
		
		$predef = $queue->getPredef();
		$settings = $this->settings['predef'][$predef];
		
		if ($settings['events']['queueRegistered']) {
			$queueSettings = $this->getQueueSettings($queue);
			$queueSettings['recipients'] = $this->anyHelper->callClassFunction( $queueSettings['recipientProvider']['class'], $queueSettings['providerParams'] );
			$this->callEventListeners( $settings['events']['queueRegistered'], $queueSettings );
		}
			
		if ($params['queue'] === false || ($params['executioninterval'] && $params['execute'])) $this->processQueue( $uid );
		
		return $queue;
	}
		
		
	/**
	*	Holt alle Einstellungen für einen Queue, z.B. recipientProvider, contentProvider etc.
	*
	*	@var $queue		der Queue
	* 	@return array
	*/
	public function getQueueSettings( $queue = null ) {

		// Art der Benachrichtigung, z.B. "mail" - später auch "push"
		$transportType = $queue->getTransport();
		$queueUid = $queue->getUid();
	
		// Voreinstellungen aus TS holen (settings.predef.XXX)
		$predef = $queue->getPredef();
		$settings = $this->settings['predef'][$predef];				
		$transportSettings = $settings['transport'][$transportType];
		if ($subject = $queue->getSubject()) $transportSettings['subject'] = $subject;
			
		// Custom-Parameter (In DB: als JSON), die beim Aufruf der Provider übergeben werden sollen
		$sendparams = $queue->getSendparams();
	
		// Provider für die Empfänger-Adressen
		$recipientProvider = array('class'=>$queue->getRecipientProvider());
		if (!$recipientProvider['class']) $recipientProvider = $transportSettings['recipientProvider'];
		if (!$recipientProvider['class']) $recipientProvider = $settings['recipientProvider'];

		// Provider für den Content der Nachricht
		$contentProvider = array('class'=>$queue->getContentProvider());
		if (!$contentProvider['class']) $contentProvider = $transportSettings['contentProvider'];
		if (!$contentProvider['class']) $contentProvider = $settings['contentProvider'];
	
		// Provider für die Daten einer einzelnen Nachricht
		$dataProvider = array('class'=>$queue->getDataProvider());
		if (!$dataProvider['class']) $dataProvider = $transportSettings['dataProvider'];
		if (!$dataProvider['class']) $dataProvider = $settings['dataProvider'];
	
		// Parameter, die an alle Provider übergeben werden:
		$providerParams = array(
			'queue'			=> $queue,
			'transport'		=> $transportType,
			'params' 		=> $sendparams, 
			'settings' 		=> $settings,
			'config'		=> $transportSettings,
			'provider'		=> array(),
			'TSFE'			=> &$GLOBALS['TSFE'],
			'baseUrl'		=> $this->settingsUtility->getBaseUrl(),
			'domain'		=> $this->settingsUtility->getDomain()
		);
		
		return array(
			'transportType' 	=> $transportType,
			'transportSettings' => $transportSettings,
			'settings'			=> $settings,
			'recipientProvider' => $recipientProvider,
			'contentProvider'	=> $contentProvider,
			'dataProvider'		=> $dataProvider,
			'providerParams'	=> $providerParams
		);
	}
	
	
	/**
	*	Arbeitet die offenen Queues ab
	*	index.php?eID=nnnotifications&action=processQueue
	*
	*	@var int $uid		Optional: Einzelnen, bestimmten Queue ausführen
	* 	@return boolean
	*/
	public function processQueue( $uid = null ) {

		if (!$uid) {
			// Alle offenene Queues laden
			$allQueues = $this->queueRepository->findAll();
		} else {
			$allQueues = array($this->queueRepository->findByUid( $uid ));
		}
			
		if (count($allQueues)) {
		
			foreach ($allQueues as $queue) {

				$queueUid = $queue->getUid();			
				$queueSettings = $this->getQueueSettings($queue);				

				$settings			= $queueSettings['settings'];
				$transportType 		= $queueSettings['transportType'];
				$transportSettings 	= $queueSettings['transportSettings'];
				$providerParams		= $queueSettings['providerParams'];
				
				$recipientProvider	= $queueSettings['recipientProvider'];
				$contentProvider	= $queueSettings['contentProvider'];
				$dataProvider		= $queueSettings['dataProvider'];

				// Prüfen, ob es sich um eine wiederholende Aussendung in bestimmten Intervallen handelt
				if ($executionInterval = $queue->getExecutioninterval()) {
					$lastExecution = $queue->getLastexecution();
					if ($lastExecution != 0 && mktime() - $lastExecution < $executionInterval) {
						// Nichts machen, mit nächsten Queue fortfahren
//						echo "nein";				
						continue;
					}
//					echo "ja";
				}
				
				// Empfänger-Liste vom Provider holen				
				$recipients = $this->anyHelper->callClassFunction( $recipientProvider['class'], $providerParams);

				// Empfänger aus Liste entfernen, wenn sie bereits eine Nachricht mit der queue.uid erhalten haben
				$recipients = $this->logRepository->removeRecipientsIfLogged( $queueUid, $recipients );
				
				$queueCompleted = count($recipients) == 0;
				
				if (!$queueCompleted) {

					// Beschränkung der Zahl an Aussendungen pro scheduler-Aufruf?
					if ($limit = $transportSettings['maxNotificationsPerBatch']) {
						$queueCompleted = count($recipients) <= $limit;
						$recipients = array_slice( $recipients, 0, min($limit, count($recipients)) );
					} else {
						$queueCompleted = true;
					}

					// Für jeden Empfänger eine Nachricht generieren				
					foreach ($recipients as $recipient) {

						if (is_string($recipient)) $recipient = array('recipient' => $recipient);
						if (!trim($recipient['recipient'])) continue;
					
						$providerParams['recipient'] = $recipient;
						
						// individueller Datensatz (z.B. aus Tabelle fe_users) für Nachricht holen
						if ($dataProvider) {
							$providerParams['provider'] = $dataProvider;
							$notificationData = $this->anyHelper->callClassFunction( $dataProvider['class'], $providerParams );
						} else {
							$notificationData = $recipient;
						}
						
						// Nachricht über angegebenen contentProvider rendern
						$content = '';
						if ($contentProvider) {
							$providerParams['provider'] = $contentProvider;
							$providerParams['data'] = $notificationData;
							$content = $this->anyHelper->callClassFunction( $contentProvider['class'], $providerParams );
						}
						
						// Nachricht über angegebenen Transporter (z.B. "mail") versenden
						$logUid = $this->logRepository->log( $queueUid, $recipient['recipient'], 1 );
						$providerParams['content'] = $content;
						$status = $this->anyHelper->callClassFunction( $transportSettings['class'], $providerParams );
						if ($status) $this->logRepository->setError( $logUid, 0 );
					
					}
				
					// Zähler der Gesamt-Aussendungen erhöhen
					$queue->setTotalsent( $queue->getTotalsent() + count($recipients) );
					$this->queueRepository->update($queue);
					
					$this->anyHelper->simpleLog( "[queue: {$queueUid}]: ".($transportSettings['disableSend'] ? ' -DEMO, kein Versand- ' : '').' Versand an '.count($recipients).' Empfänger.' );
				
				}
								
				// Wurde Queue komplett beendet?
				if ($queueCompleted) {
				
					// Update der letzten Ausführung
					$queue->setLastexecution( mktime() );
					$this->queueRepository->update($queue);
					
					// Nur wenn es kein Interval gibt, Queue aus Liste entfernen
					if (!$queue->getExecutioninterval()) {
						$this->queueRepository->remove( $queue );
					}
					
					$this->anyHelper->simpleLog( "[queue: {$queueUid}]: Vollständing beendet." );
					
					// optionale Aktionen nach dem vollständigen Versand aufrufen
					$this->callEventListeners( $settings['events']['finalize'], $queue );

				}

				$this->persistenceManager->persistAll();
			}
						
		}
		
		
//		DebugUtility::debug($log);
		
		return true;
	}
	
	
	
	public function callEventListeners ( $actions, $params ) {
		if (!$actions) return;
		foreach ($actions as $action) {
			$this->anyHelper->callClassFunction( $action['class'], $params );
		}
	}
	
	/**
	*	Löscht alle Log-Einträge für einen Queue
	*
	* 	@return void
	*/
	public function clearLogEntriesForQueue( $queue ) {
		if (!$queue) return;
		$this->logRepository->clearLogsForQueue( $queue->getUid() );
	}
	
}


?>