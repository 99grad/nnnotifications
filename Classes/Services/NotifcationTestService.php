<?php

namespace Nng\Nnnotifications\Services;

use Nng\Nnnotifications\Services\AbstractService;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/*

	Neuen Queue anlegen:
	http://irbd.99grad.de/index.php?eID=nnnotifications&action=addTestToQueue

	Queue starten
	http://irbd.99grad.de/index.php?eID=nnnotifications&action=processQueue
	
*/

class NotifcationTestService extends AbstractService {
	
	
	/**
	*	Generiert viele Einträge in der Log-Datenbank
	*	index.php?eID=nnnotifications&action=createTestData
	*
	* 	@return void
	*/
	public function createTestData( $gp = array() ) {
		for ($i = 0; $i < 100000; $i++) {
			$this->logRepository->log('4', 'david-'.$i.'@99grad.de', 0);
		}		
		echo "Test-Daten in Log-DB erstellt";
	}
	
	
	/**
	*	Macht einen Performance-Test verschiedener Ansätze beim Laden von Daten aus der Log-Datenbank
	*	index.php?eID=nnnotifications&action=performanceTest
	*
	* 	@return void
	*/
	public function performanceTest( $gp = array() ) {
		$this->timer();
		$emails = $this->getTestRecipients();
		echo count($emails).' Adressen in Array - ' . $this->timer().'<br />';
		$recipients = $this->logRepository->removeRecipientsIfLogged( 4, $emails );
		echo count($recipients).' Filtern (removeRecipientsIfLogged) - ' . $this->timer().'<br />';
		//print_r($recipients);
	}


	/**
	*	Funktionen zum Tracken der Zeit bei großen Abfragen
	*
	* 	@return void
	*/
	
	public function getMicrotime () {
		$mk = round(microtime(true) * 1000);
		return $mk/1000;
	}
	
	
	public function timer () {
		if (!$this->t) {
			$this->t = $this->getMicrotime();
			return '';
		}
		$t = $this->t;
		$this->t = $this->getMicrotime();
		return $this->getMicrotime()-$t;
	}

	
	
	/**
	*	Versendet eine Testmail
	*	index.php?eID=nnnotifications&action=sendTestMail
	*
	* 	@return void
	*/
	public function sendTestMail( $gp = array() ) {
		
		$mailSettings = $this->settings['predef']['test']['transport']['mail'];
				
		ArrayUtility::mergeRecursiveWithOverrule( $mailSettings, array(
			'html'	=> $this->anyHelper->renderTemplate( 'typo3conf/ext/nnnotifications/Resources/Private/Templates/Tests/TestMail.html', $mailSettings ),
			'data'	=> $mailSettings
		));
	
		$this->emailService->send( $mailSettings );
	}
	
	
	
	/**
	*	Legt einen Test-Versand an, der vom scheduler abgeholt wird
	*	index.php?eID=nnnotifications&action=addTestToQueue
	*
	* 	@return void
	*/
	public function addTestToQueue( $params = array() ) {
	
		$params = array(
		
			'transport'			=> 'mail',
			'subject' 			=> 'Test '.date('H:i:s'),
			'title'				=> 'Test über addTestToQueue()',
			
			// einzelne Provider ODER predef definieren (contentProvider hat Vorrang). Predef Definition in setup.txt
			/*
			'recipientProvider' => '\Nng\Nnnotifications\Services\NotifcationTestService->getTestRecipients',
			'dataProvider'		=> '\Nng\Nnnotifications\Services\NotifcationTestService->getDataForRecipient',
			'contentProvider'	=> '\Nng\Nnnotifications\Services\NotifcationTestService->getTestContent',
			//*/
			//'predef'			=> 'nnforum_newpost',
			'predef'			=> 'test',
			
			// Queue erstellen für scheduler (true) oder Versand direkt ausführen (false)
			'queue'				=> true,
			
			// Beispiel für eine wiederholende Benachrichtigung: Direkt benachrichtigen, (execute = true) – dann alle X Sekunden
			//'executioninterval'	=> 60*30,
			//'execute'			=> true,
			
			// Zusätzliche eigene Parameter, werden an die einzelnen Provider weitergeschleust
			'sendparams' 	=> array(
				'a' => 1
			)
		);
		
		$queue = $this->queueService->addToQueue( $params );
		
		if ($queue) {
			echo "Neuer Queue mit <b>uid=". $queue->getUid() . '</b> angelegt.<br />';
			if ($params['queue']) {
				echo "Versand wurde in scheduler-Warteschlange gesetzt (queue = true) und kann manuell über das Backend oder über index.php?eID=nnnotifications&action=processQueue aufgerufen werden";
			} else {
				echo "Versand wurde direkt angestossen (queue = false)";
			}
		}
					
	}
	
}


?>