<?php

namespace Nng\Nnnotifications\Services;

use TYPO3\CMS\Core\SingletonInterface;


abstract class AbstractService implements SingletonInterface {
	
	/**
	* @var \TYPO3\CMS\Extbase\Object\ObjectManager
	* @inject
	*/
	protected $objectManager;
	
	/**
	* @var \Nng\Nnnotifications\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	
	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	/**
	 * TypoScript settings
	 * @var array
	 */
	protected $settings;
	
	/**
	 * TypoScript setup
	 * @var array
	 */
	protected $TsSetup;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;
	
	/**
	 * @var \Nng\Nnnotifications\Services\EmailService
	 * @inject
	 */
	protected $emailService;
	
	/**
	 * @var \Nng\Nnnotifications\Services\QueueService
	 * @inject
	 */
	protected $queueService;
	
	/**
	 * @var \Nng\Nnnotifications\Services\NotifcationTestService
	 * @inject
	 */
	protected $notifcationTestService;
	
	/**
	* @var \Nng\Nnnotifications\Domain\Repository\QueueRepository
	* @inject
	*/
	protected $queueRepository;
	
	
	/**
	* @var \Nng\Nnnotifications\Domain\Repository\LogRepository
	* @inject
	*/
	protected $logRepository;
	
	/**
	* @var \Nng\Nnnotifications\Domain\Repository\NotificationDataRepository
	* @inject
	*/
	protected $notificationDataRepository;
	
	
	public function initializeObject () {
		$this->TsSetup = $this->settingsUtility->getTsSetup();
		$this->settings = $this->TsSetup['settings'];
	}
	
}
		