<?php

namespace Nng\Nnnotifications\Services;

use Nng\Nnnotifications\Services\AbstractService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class EmailService extends AbstractService {
	
	
	/**
	 * @var \TYPO3\CMS\Core\Mail\MailMessage
	 * @inject
	 */
	protected $mail;
	
	/**
	* @var \TYPO3\CMS\Extbase\Object\ObjectManager
	* @inject
	*/
	protected $objectManager;
	
	/**
	* @var \Nng\Nnnotifications\Utilities\SettingsUtility
	* @inject
	*/
	protected $settingsUtility;
	
	/**
	* @var \Nng\Nnnotifications\Helper\AnyHelper
	* @inject
	*/
	protected $anyHelper;
	
	/**
	* @var \TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator
	* @inject
	*/
	protected $emailAddressValidator;
	
	
	
	/* ==================================================================================================
		Versand der Mail über SWIFT-Mailer
		
		$params	.fromEmail		=>	Absender E-Mail
				.fromName		=>	Absender Name
				.toEmail		=>	Array mit Empfängern der E-Mail (auch kommasep. Liste möglich)
				.subject		=>	Betreff der E-Mail
				.attachments	=>	Array mit vollständigem Pfad der Anhänge
									geht auch per ViewHelper: <VH:attachment src="..." embed="1" />
				.inlineImages	=>	Array mit Pfad der Bilder, die als Inline-Image gesendet werden sollen
									geht auch per ViewHelper: <VH:image src="..." embed="1" alt="Bildname" />
				.html			=>	HTML-Part der Mail
				.template		=>	Alternativ: Pfad zum Template, das gesendet werden soll
				.data			=>	Wenn .template angegeben wurde: Variablen für fluid-template
				.plaintext		=>	Plain-Text Mail
				.baseURL		=>	baseURL, für eingebettete Bilder
				
		http://docs.typo3.org/TYPO3/CoreApiReference/ApiOverview/Mail/Index.html
		http://swiftmailer.org/docs/messages.html
	*/


	function send ( $params ) {
		
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');
		if (!$params['data'])		$params['data'] = array();
				
		$mail->setFrom(array( $params['fromEmail'] => $params['fromName'] ));
		
		$recipients = array();
		$recipientsNames = array();
		
		$recipientsArr 		= $this->anyHelper->trimExplodeArray($params['toEmail']);
		$recipientsNameArr 	= $this->anyHelper->trimExplodeArray($params['toName']);

		foreach ($recipientsArr as $n=>$recipient) {
			if (!$this->emailAddressValidator->validate($recipient)->getErrors()) {
				$name = count($recipientsNameArr) >= $n ? $recipientsNameArr[$n] : '';
				if ($name = trim($name)) {
					$recipients[$recipient] = $name;
				} else {
					$recipients[] = $recipient;
				}
			}
		}

		if (!$recipients) return false;

		$mail->setTo($recipients);
		$params['subject'] = $this->anyHelper->renderTemplateSource( $params['subject'], $params );
		$mail->setSubject($params['subject']);
		
		if (!$params['baseURL']) $params['baseURL'] = $this->settingsUtility->getBaseURL();

		// --------------------------------------
		// File-Attachments an Mail hängen?
		
		$attachments = $this->anyHelper->trimExplodeArray($params['attachments']);
		if ($GLOBALS['nnnotifications_attachments']) $attachments = array_merge($attachments, $GLOBALS['nnnotifications_attachments']);
		if ($attachments) {
			foreach ($attachments as $path) {
				$path = $this->settingsUtility->getPathToFile( $path );
				if ($path) {
					$attachment = \Swift_Attachment::fromPath($path);
					$mail->attach($attachment);
				}
			}
		}
		
		// -----------------------------------------------------------
		// Template rendern?
				
		$html = $params['html'];

		if (!$html && $params['template']) {
			\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($params['data'], array(
				'baseUrl' => $this->settingsUtility->getBaseURL(),
				'domain' => $this->settingsUtility->getDomain(),
				'settings' => $this->settings,
				'TSFE'		=> array('id'=>$GLOBALS['TSFE']->id)
			));
			$html = $this->anyHelper->renderTemplate( $params['template'], $params['data'] );
		}

		$plaintext = $params['plaintext'] ? $params['plaintext'] : $this->html2plaintext($params['html']);


		// -----------------------------------------------------------		
		// Inline-Bilder an Mail hängen?
		
		$inlineImages = $this->anyHelper->trimExplodeArray($params['inlineImages']);
		if ($GLOBALS['nnnotifications_inlineimages']) $inlineImages = array_merge($inlineImages, $GLOBALS['nnnotifications_inlineimages']);

		//$this->anyHelper->logFile('CRON.txt', print_r($inlineImages, true) );		
		//$this->anyHelper->logFile('CRON.txt', print_r($params, true) );
		
		if ($inlineImages && !$params['outputInBrowser']) {
			foreach ($inlineImages as $img) {
				$img = str_replace(PATH_site, '', $img);
				if (file_exists(PATH_site.$img) && strpos($html, $img) !== false) {
					$cid = $mail->embed(\Swift_Image::fromPath(PATH_site.$img));
					$pathsToReplace = array(
						PATH_site.$img,
						$params['baseURL'].$img,
						str_replace('http:', 'https:', $params['baseURL']).$img,
						GeneralUtility::getIndpEnv('TYPO3_SITE_URL').$img,
						$img
					);
//					$this->anyHelper->logFile('CRON.txt', print_r($pathsToReplace, true) );
					$html = str_replace( $pathsToReplace, $cid, $html);
//					$this->anyHelper->logFile('CRON.txt', $html );
				}
			}
		}
		
		// -----------------------------------------------------------
		// Parsed das CSS und hängt es als inline-Styles an die Tags
		// https://packagist.org/packages/pelago/emogrifier
		
		if ($params['css2inline']) {
			
			$doc = \phpQuery::newDocumentHTML($html);
			$css = trim(pq('style')->html());
			
			if ($css) {
				$emogrifier = new \Pelago\Emogrifier();
				$emogrifier->setHtml($html);
				$emogrifier->setCss($css);
				if (method_exists( $emogrifier, 'disableStyleBlocksParsing' )) {
					$emogrifier->disableStyleBlocksParsing();
				}
				$html = $emogrifier->emogrify();
			}
			
		}
				
		if ($html) {
			$mail->setBody($html, 'text/html');
			$mail->addPart($plaintext, 'text/plain');	
		} else {
			$mail->setBody($plaintext, 'text/plain');
		}

		if ($params['returnPath']) {
			$mail->setReturnPath( $params['returnPath'] );
		} else if ($defaultMailFromAddress = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']) {
			$mail->setReturnPath( $defaultMailFromAddress );
		}
		
		// -----------------------------------------------------------
		// Versenden der Mail – oder Ausgabe im Browser
				
		if ($params['outputInBrowser']) {
			$html = str_replace('src="', 'src="'.$params['baseURL'], $html);
			$html = str_replace('<head>', '<head><base href="'.$params['baseURL'].'">', $html);		
			echo $html;
		}

		if ($params['disableSend']) {
			$this->anyHelper->addFlashMessage ( 'Nnnotifications', 'Demo-Modus - disableSend war gesetzt, keine Mail an <b>'.join(',', $recipients).'</b> versendet.', 'NOTICE');
			$sent = true;
		} else {
			$sent = $mail->send();
		}

		$GLOBALS['nnnotifications_inlineimages'] = array();
		$GLOBALS['nnnotifications_attachments'] = array();
		
		// -----------------------------------------------------------
		// Log-Files

		if (!$sent && $params['errorLogFile']) {
			$this->anyHelper->logFile($params['errorLogFile'], join(',', array_keys($recipients) ));
		}
		
		if ($sent && $params['successLogFile']) {
			$str = ($params['disableSend'] ? ' --DEMO-- ' : ' ').join(',', array_keys($recipients));
			$this->anyHelper->logFile($params['successLogFile'], $str);
		}
		
		return $sent;
	}
	
	/* ==================================================================================================
	*/
	
	function html2plaintext ( $html ) {
		$plainMessage = strip_tags($html);
		return $plainMessage;
	}
		
		
}
		