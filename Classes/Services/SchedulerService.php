<?php

namespace Nng\Nnnotifications\Services;

use Nng\Nnnotifications\Services\AbstractService;
use TYPO3\CMS\Core\Utility\ArrayUtility;

class SchedulerService extends AbstractService {
	
	/**
	* @return void
	*/
	public function executeSchedulerTask() {
		$this->queueService->processQueue();
		return true;
	}
	
}


?>