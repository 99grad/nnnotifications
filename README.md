# Notification Queue for Typo3 (nnnotification) #

Typo3-Extension zur Vereinfachung von Massen-Mails per E-Mail (später auch Push, SMS etc). Die Extension ist als zentraler Dienst für andere Extensions gedacht und lässt sich problemlos erweitern und anbinden.

### Allgemeine Features ###

* Queue-Service: Versenden von Nachrichten über eigenen __Scheduler-Task__ um PHP-Timeouts zu vermeiden
* __Backend-Modul__: Übersicht der laufenden Prozesse
* __Wiederholender Versand__: Per Interval möglich um z.B. 1x im Monat einen Versand zu triggern
* Batch-Limitierung: Begrenzung der Zahl an Aussendungen pro Aufruf um Server-Begrenzungen zu vermeiden
* Keine Duplikate: Jede Nachricht wird nur 1x zugestellt, auch bei Abbruch des Scriptes oder mehrfachem Aufruf
* Log: Ins Protokoll und/oder in eine Textdatei möglich
* Anbindung an jede beliebige Datenquelle zum Generieren der Empfängerlisten
* Einfache Erweiterbarkeit: Eigene Templates und Versand-Methoden
* Events: Einrichten von verschiedenen Event-Listenern (z.B. beim Start und Beenden eines Queues)

### Features des Mail-Transporters ###
* inline CSS: Automatisches Konvertieren von CSS-Dateien in __inline-Style-Attribute__ (verbessert die Darstellung in älteren Mail-Clients)
* inline Images: Eigener `ImageViewHelper` mit Parameter `embed=1` bindet Bilder auf Wunsch als Inline-Bilder in Mail ein
* Datei-Anhänge: `AttachmentViewHelper` zum bequemen __Anhängen von Dateien__ (PDFs etc.) an die Mail

### Installation ###

* Extension installieren
* Scheduler-Task für NN Notification anlegen, empfohlener Aufruf: Alle 60 Sekunden
* CrobJob (Server/Host) für Scheduler einrichten, falls noch nicht geschehen

***
# Hintergrund #

Der Ablauf am konkreten Beispiel: Beim Erstellen eines neuen Blog-Beitrages sollen automatisch alle Abonnenten des Blogs benachrichtigt werden. Bei wenigen Empfängern lässt sich das über eine einfache `foreach`-Schleife lösen. Wenn es aber sehr viele Empfänger werden, kann es zu einem TimeOut eines normalen PHP-Scriptes kommen oder man gerät an die  Begrenzung der vom Host/Server eingestellten maximal-Aussendungen pro Stunde. Hier hilft der Queue-Service:

## Ablauf ##
* Nach dem Speichern des neuen Blog-Beitrags wird der QueueService aufgerufen und ein neuer Queue in der Tabelle `tx_nnnotifications_domain_model_queue` angelegt
* Der Scheduler stößt den QueueService jede Minute an
* Der Queue-Service prüft zunächst, ob es offene Aufträge gibt
* Bei jedem Auftrag ruft er zuerst den `recipientProvider` auf, der ihm eine Liste aller Empfänger zurückgibt
* Aus der Liste werden alle Empfänger entfernt, die bereits die Nachricht bekommen haben
* Falls definiert, wird die Liste auf die max. zulässige Zahl an Empfängern per Batch gekürzt
* Für jedem Empfänger ruft der Queue-Service den `dataProvider` auf, der ihm personalisierte Daten zum Empfänger zurückliefert
* Die Daten aus dem `dataProvider` übergibt der QueueService an den `contentProvider`, der daraus die eigentliche Nachricht rendert
* Die Nachricht wird an den `transporter` übergeben, der sich um den eigentlichen Versand (z.B. per Mail) kümmert
* Für jede versendete Nachricht wird ein Eintrag in der Tabelle `tx_nnnotifications_domain_model_log` gemacht
* Jeder Versand an einen Empfänger wird in der Tabelle `tx_nnnotifications_domain_model_log`protokolliert
* Wenn der Versand abgeschlossen ist, wird die Tabelle `tx_nnnotifications_domain_model_log` automatisch geleert

***
# Einfaches Beispiel mit TypoScript Setup #

Innerhalb der eigenen Extension im __Setup__ eine Voreinstellung für den Versand festlegen (hier `beispielSetup`). Das Setup sollte die Voreinstellungen von `plugin.tx_nnnotifications.settings.predef.default` erben:

```
plugin.tx_nnnotifications.settings.predef {
	beispielSetup < plugin.tx_nnnotifications.settings.predef.default
	beispielSetup {
		contentProvider.template = typo3conf/ext/beispiel/Resources/Private/Notifications/NewPost.html
		recipientProvider.class = \Nng\Beispiel\Service\NotificationService->getRecipients
		dataProvider.class = \Nng\Beispiel\Service\NotificationService->getData
		
		transport.mail {
			maxNotificationsPerBatch = 30
			fromEmail = david@99grad.de
			fromName = Forum
		}

	}
}
```

Und die entsprechende __Provider-Klasse__ dazu:

```
<?php

namespace Nng\Beispiel\Service;

class NotificationService {
	
	/**
	*	Alle Empfänger für die Benachrichtigung holen
	*	Aufruf von \Nng\Nnnotifications\Services\QueueService
	*/
	public function getRecipients ( $params ) {		
		return array(
			array('uid'=>'1', 'recipient'=>'david@99grad.de'),
		);
	}
	
	/**
	*	Daten für die Nachricht an den einzelnen Empfänger holen
	*	Aufruf von \Nng\Nnnotifications\Services\QueueService
	*/	
	public function getData ( $params ) {
		return array(
			'titel'	=> 'beispiel'
		);
	}

}
```

Die neue Nachricht kann per Aufruf des __QueueServices__ in den Versand-Queue geschrieben werden:

```
$params = array(
	'transport'	=> 'mail',
	'title'		=> 'Ein Beispiel',
	'predef'	=> 'beispielSetup',
	'sendparams'	=> array(),
	// Nachricht nicht in Warteschlange, sondern direkt senden:
	//'queue'	=> false,
);

$queueService = $this->objectManager->get('\Nng\Nnnotifications\Services\QueueService');
$queueService->addToQueue( $params );
```


***
# Provider-Typen #
## recipientProvider ##

Der __recipientProvider__ liefert die vollständige Liste der Empfänger als Array. 
Die Rückgabe kann ein __einfaches Array__ sein z.B. 
```
array('david@99grad.de', 'jan@99grad.de', 'peter@99grad.de', ...)
```

...oder ein __Array mit E-Mail als Key und Name als Value__:
```
array('david@99grad.de'=>'David Bascom', 'jan@99grad.de'=>'Jan Peters', 'peter@99grad.de'=>'Peter Olafson', ...)
```

...oder ein mehrdimensionales __assoziatives Array__, z.B.
```
array(
	array('uid'=>1, 'recipient'=>'david@99grad.de', 'name'=>'David Bascom'),
	array('uid'=>2, 'recipient'=>'jan@99grad.de', ...),
	...
)
```


## dataProvider ##

Der __dataProvider__ wird vor dem Generieren jeder einzelnen Nachricht aufgerufen. Er gibt ein Array mit Variablen zurück, die beim Rendern des Inhalts an den `contentProvider` übergeben werden und z.B. für das Rendern des Fluid-Template verwendet werden. Klassischerweise sind das personalisierte Daten wie Felder aus der fe_users-Tabelle, um die Nachricht mit personalisierter Ansprache zu machen.

Soll einfach ein Datensatz aus einer vorhandenen Tabelle geladen werden, kann das auch per TypoScript erledigt werden. Da dem `dataProvider` jeweils der Datensatz eines einzelnen Empfängers aus der `recipientProvider`-Liste übergeben wird, kann man die Felder zur Abfrage in der Datenbank nutzen:
```
plugin.tx_nnnotifications.settings.predef {
	...
	beispielSetup {
		...
		dataProvider {
			class = \Nng\Nnnotifications\Provider\DataProvider->getFromTable
			tablename = fe_users
			where { 
				email = {field:recipient}
				# Kann auch z.B. die fe_users.uid sein:
				#uid = {field:uid}
			}
		}
		...
	}
}
```

## contentProvider ##

Der __contentProvider__ erhält die Daten aus dem `dataProvider` und gibt die gerenderte Nachricht zurück. Wird als `transport` die Voreinstellung `mail` verwendet, sollte der contentProvider ein array mit dem Attribut `html` zurückgeben: `array('html'=>'...')`

Soll einfach ein Fluid-Template gerendert werden, kann dazu der Default-Content-Provider `\Nng\Nnnotifications\Provider\ContentProvider->getFromTemplate` genutzt werden. Wie im Beispiel-Setup oben, reicht beim Erben der default-Settings ein Überschreiben von `contentProvider.template`. Die ausführliche Version wäre:
```
plugin.tx_nnnotifications.settings.predef {
	...
	beispielSetup {
		...
		contentProvider {
			class = \Nng\Nnnotifications\Provider\ContentProvider->getFromTemplate
			template = typo3conf/ext/nnnotifications/Resources/Private/Templates/Test/Mail.html
		}
		...
	}
}
```


***
# Queue-Service #
## Parameter beim Registrieren eines Queue-Auftrages ##
```
$params = array(
	
	// Der Transport-Typ, definiert in settings.predef.default.transport.[xxxx] 
	'transport'			=> 'mail',
	
	// Default Betreff, z.B. für Mail 
	'subject' 			=> 'Test '.date('H:i:s'),

	// Titel des Queues für Übersicht im Backend
	'title'				=> 'Test über addTestToQueue()',
	
	// einzelne Provider ODER predef definieren (contentProvider hat Vorrang). Predef Definition: siehe setup.txt
	/*
	'recipientProvider' => '\Nng\Nnnotifications\Services\NotifcationTestService->getTestRecipients',
	'dataProvider'		=> '\Nng\Nnnotifications\Services\NotifcationTestService->getDataForRecipient',
	'contentProvider'	=> '\Nng\Nnnotifications\Services\NotifcationTestService->getTestContent',
	*/
	'predef'			=> 'test',
	
	// Queue erstellen für scheduler (true) oder Versand direkt ausführen (false)
	'queue'				=> true,
	
	// Beispiel für eine wiederholende Benachrichtigung: Direkt benachrichtigen, (execute = true) – dann alle X Sekunden
	//'executioninterval'	=> 60*30,
	//'execute'			=> true,
	
	// Zusätzliche eigene Parameter, werden an die einzelnen Provider weitergeschleust
	'sendparams' 	=> array(
		'a' => 1
	)
);

$queueService = $this->objectManager->get('\Nng\Nnnotifications\Services\QueueService');
$queue = $queueService->addToQueue( $params );
```

***
# Weitere Einstellungen #
## Auszug aus dem TypoScript-Setup, der weitere Einstellungen dokumentiert ##
```
plugin.tx_nnnotifications {

	settings {
		
		predef {
			
			# ------------------------------------------------------------------------------------
			# Default-Konfigurationen
			#
			# Alle eigenen Konfigurationen sollte diese Einstellungen erben:
			# configname < plugin.tx_nnnotifications.settings.predef.default
			# ------------------------------------------------------------------------------------
			
			default {
			
				# Klasse->Methode, die den Inhalt der Mail generiert
				contentProvider {
					# Default ist der Fluid Template Renderer
					class = \Nng\Nnnotifications\Provider\ContentProvider->getFromTemplate
					template = typo3conf/ext/nnnotifications/Resources/Private/Templates/Test/Mail.html
				}
				
				# Klasse->Methode, die die Liste der Empfänger generiert
				recipientProvider {
					class = \Nng\Nnnotifications\Provider\RecipientsProvider->getRecipients
				}
				
				# Klasse->Methode, die individuelle Daten für einzelne Nachricht aus DB holt (z.B. für die Personalisierung)
				dataProvider {
					# Beispiel, wie anhand einer Liste von übergebenen E-Mail-Adressen der passende fe_user gefunden wird
					#class = \Nng\Nnnotifications\Provider\DataProvider->getFromTable
					#tablename = fe_users
					#where { 
					#	email = {field:recipient}
					#	uid = {field:uid}
					#}
				}
				
				# Die Sende-Verfahren (mail, push etc.) und individuelle Konfigurationen 
				transport {
					mail {
						class = \Nng\Nnnotifications\Transport\EmailTransport->send
										
						toEmail = david@99grad.de
						fromEmail = david@99grad.de
						fromName = David Bascom
						subject = NN Notification
						returnPath = 
				
						# Inline-Bilder: Können auch direkt über ViewHelper eingebunden werden, siehe TestMail.html
						#inlineImages = typo3conf/ext/nnnotifications/Resources/Public/Imgs/test-logo.jpg
				
						# Datei-Anhänge: Können auch direkt über ViewHelper eingebunden werden, siehe TestMail.html
						#attachments = typo3conf/ext/nnnotifications/Resources/Public/Attachments/test-anhang.pdf
						
						# Maximale Aussendungen pro Zyklus (Scheduler-Aufruf)
						maxNotificationsPerBatch = 50
			
						# Alle Style-Tags zusätzlich als inline-Style an die HTML-Tags
						css2inline = 1
			
						# Für Debugging: Ausgabe der Mail direkt in Browser
						outputInBrowser = 0

						# Für Debugging: "echten" Versand deaktivieren (aber z.B. loggen, wenn successLogFile gesetzt ist)
						disableSend = 0
			
						# Log in Datei ausgeben?
						errorLogFile = typo3conf/nnnotifications_mail_error.txt
						successLogFile = typo3conf/nnnotifications_mail.txt
						
						# Die oben definierten Provider können hier überschrieben werden
						#recipientProvider.class = \Nng\Nnnotifications\Tests\RecipientProvider->getRecipients
						#contentProvider.class = \Nng\Nnnotifications\Tests\ContentProvider->getContent
						#dataProvider.class = \Nng\Nnnotifications\Tests\DataProvider->getDataForRecipient
					}
				}
				
				events {
					# Nachdem der Queue registriert wurde (in der DB eingetragen ist)
					queueRegistered {
					}
					# Nach dem Versenden aller Nachrichten eines Queues
					finalize {
						# Log-Einträge aus Tabelle tx_nnnotifications_domain_model_log löschen
						# Sollte immer gesetzt sein, um DB klein zu halten
						# MUSS gesetzt sein, wenn ein Interval eingestellt wurde!!
						1.class = \Nng\Nnnotifications\Services\QueueService->clearLogEntriesForQueue
					}
				}				
			}
		}
	}
}
```