<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

// --- Get extension configuration ---
$extConf = array();
if ( strlen($_EXTCONF) ) {
	$extConf = unserialize($_EXTCONF);
}

// Beispiel für Einbindung bestimmter Elemente, nur unter Bedingung
/*
if ( isset($extConf['enableGridSimpleRow']) && $extConf['enableGridSimpleRow'] ) {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:nnbase/Configuration/TypoScript/simpleRow/tsconfig.ts">');
}
*/


// -----------------------------------------------------------------------------------
// eID Dispatcher

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['nnnotifications'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnnotifications').'Classes/Dispatcher/EidDispatcher.php';

// ------------------------------------------------------------------------------------------------
// Externe Libraries

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath( $_EXTKEY ) . '/Resources/Libraries/vendor/autoload.php';


// ------------------------------------------------------------------------------------------------
// Scheduler Task
// Manueller Aufruf: /usr/local/bin/php /is/htdocs/wp1075974_8KRC2LBGVR/projekte/irbd/typo3/cli_dispatch.phpsh scheduler

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Nng\Nnnotifications\Tasks\SchedulerTask'] = array(
        'extension' => $_EXTKEY,
        'title' => 'NN Notifications',
        'description' => 'Versenden von Benachrichtungen aus dem nnnotifications-Queue.'
);

?>