<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

// --- Get extension configuration ---
$extConf = array();
if ( strlen($_EXTCONF) ) {
	$extConf = unserialize($_EXTCONF);
}

// Add static typoscript configurations
// Default stuff and grid definitions
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'NN Website Basis');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nnnotifications_domain_model_queue', 'EXT:nnnotifications/Resources/Private/Language/locallang_csh_tx_test_domain_model_queue.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nnnotifications_domain_model_queue');

$GLOBALS['TCA']['tx_nnnotifications_domain_model_queue'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:nnnotifications/Resources/Private/Language/locallang_db.xlf:tx_test_domain_model_queue',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'delete' => 'deleted',
		'enablecolumns' => array(
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Queue.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_nnnotifications_domain_model_queue.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_nnnotifications_domain_model_log', 'EXT:nnnotifications/Resources/Private/Language/locallang_csh_tx_test_domain_model_queue.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_nnnotifications_domain_model_log');

$GLOBALS['TCA']['tx_nnnotifications_domain_model_log'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:nnnotifications/Resources/Private/Language/locallang_db.xlf:tx_nnnotifications_domain_model_log',
		'label' => 'email',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'enablecolumns' => array(
		),
		'searchFields' => 'email,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Log.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_nnnotifications_domain_model_log.gif'
	),
);

if (TYPO3_MODE === 'BE') {
	Tx_Extbase_Utility_Extension::registerModule(
		'Nng.' . $_EXTKEY,
		'system',
		'mod1',
		'',
		array(
			'Mod' => 'index,delete',
		),
		array(
			'access'	=> 'user,group',
			'icon'	  => 'EXT:nnnotifications/Resources/Public/Icons/moduleicon.png',
			'labels'	=> 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod1.xlf',
		)
	);
}

?>